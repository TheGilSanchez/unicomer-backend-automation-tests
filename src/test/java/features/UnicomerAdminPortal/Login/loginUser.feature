@unicomerAdminPortal
@login_users_ap

Feature: login Unicomer Admin Portal.
  Test all scenarios of logging on Unicomer Admin Portal

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

@userNotCreated
  Scenario: Login of a user not created - C524
     When I perform Login operation to AP for "/admin-portal/login" path with body
       | parameterName | parameterValue      |
       | username      | erick@test.com      |
       | password      | 1232456789          |
    Then The response should contain the 500 status code
     And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/loginUser/wrongFields.json" file
    And The JSON response should be equal to the "/unicomerAdminPortal/jsonResponse/loginUser/wrongFields.json" file


@wrongPasswordWithUser
Scenario: Login with unauthorized User - C525
  When I perform Login operation to AP for "/admin-portal/login" path with body
    | parameterName | parameterValue               |
    | username      | cgomez@yellowpepper.com      |
    | password      | 1232456789                   |
  Then The response should contain the 401 status code


@userEmptyFields
  Scenario: Login with empty fields - C526
    When I perform Login operation to AP for "/admin-portal/login" path with body
      | parameterName | parameterValue      |
      | username      |                     |
      | password      |                     |
    Then The response should contain the 400 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/loginUser/emptyFields.json" file


  @userCreated
  Scenario: Login of a user created - C527
    When I perform Login operation to AP for "/admin-portal/login" path with body
      | parameterName | parameterValue               |
      | username      | cgomez@yellowpepper.com      |
      | password      | 201910                       |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/loginUser/userCreated.json" file

  @userInvalid
  Scenario Outline: Login with User Invalid - C528
    When I perform Login operation to AP for "/admin-portal/login" path with body
      | parameterName | parameterValue |
      | password      | 201910         |
      | username      | <user_Type>    |
    Then The response should contain the 400 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/loginUser/wrongEmail.json" file
    And The JSON response should be equal to the <type_Json> file

    Examples:
      | user_Type | type_Json                                                       |
      | asdas     |/unicomerAdminPortal/jsonResponse/loginUser/wrongEmail.json      |
      | ew.we     |/unicomerAdminPortal/jsonResponse/loginUser/wrongEmail.json      |
      |           |/unicomerAdminPortal/jsonResponse/loginUser/userWithSpaces.json  |