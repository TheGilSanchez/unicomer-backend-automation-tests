@unicomerAdminPortal
@get_users_ap

Feature: When User is logged on Unicomer Admin Portal, can get Users.
  Test all scenarios of logging on Unicomer Admin Portal

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

  @getUsersWrongParams
  Scenario Outline: get Users with wrong Parameters page and size - C587
    When I perform Login operation to AP for "/admin-portal/login" path with body
      | parameterName | parameterValue          |
      | username      | cgomez@yellowpepper.com |
      | password      | 201910                  |
    And I get Users operation to AP for "/users?page=<pageNumber>" path with "<sizeNumber>"
    Then The response should contain the 500 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/dashBoard/getUsers/wrongParameters.json" file
    And The JSON response should be equal to the "/unicomerAdminPortal/jsonResponse/dashBoard/getUsers/wrongParameters.json" file
    Examples:

      | pageNumber | sizeNumber |
      | 1          | 0          |
      | 0          | 0          |
      | -1         | -1         |

  @getUsersCorrectParams
  Scenario Outline: get Users with Correct parameters page and size - C590
    When I perform Login operation to AP for "/admin-portal/login" path with body
      | parameterName | parameterValue          |
      | username      | cgomez@yellowpepper.com |
      | password      | 201910                  |
    And I get Users operation to AP for "/users?page=<pageNumber>" path with "<sizeNumber>"
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomerAdminPortal/schemes/dashBoard/getUsers/correctParameters.json" file
    Examples:

      | pageNumber | sizeNumber |
      | 1          | 50         |
      | 1          | 10         |
      | 2          | 50         |

