@unicomer
@user

Feature: Create a User on Unicomer.
    Test all scenarios of create users on Unicomer

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

  @create_user
  Scenario: Create a new user - C67
    When I generate a unique id
     And I generate a UUI id
    And I generate fake values of a person
     And I perform a create user operation using the "users" path with body
      | parameterName  | parameterValue |
      | idDocumentType | C              |
      | devicePlatform | ANDROID        |
      | deviceModel    | huawei         |
      | country        | CR             |
      | pin            | 589437         |
      | extUId         | 0              |
    Then The response should contain the 201 status code
     And The response should contain the application/json;charset=UTF-8 content type
     And The schema of the response should be equal to the "/unicomer/schemes/createUser/createUser.json" file

  @create_user_wrong_fields
  Scenario: Create a new user with all empty fields - C103
    When I generate fake values of a person but fields required
    And I perform a create user operation using the "users" path with body and empty fields
      | parameterName  | parameterValue |
      | idDocumentType |                |
      | devicePlatform |                |
      | deviceModel    |                |
      | country        |                |
      | pin            |                |
      | extUId         |                |
    Then The response should contain the 400 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomer/schemes/createUser/createUserEmptyFields.json" file