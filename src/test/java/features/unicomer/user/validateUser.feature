@unicomer
@user
@validate_user

Feature: Validate users.
    Endpoint created to validate if an user is created or not in Unicomer

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

@userNotCreated
  Scenario: Validate user not created
    When I perform a POST request to the "users/validate" path with body
      | parameterName    | parameterValue |
      | idType           | C              |
      | idDocumentNumber | 527004420      |
      | phone            | 83123456       |
      | country          | CR             |
    Then The response should contain the 502 status code
     And The response should contain the application/json;charset=UTF-8 content type
     And The schema of the response should be equal to the "/unicomer/schemes/validateUser/userNotCreated.json" file
     And The JSON response should be equal to the "/unicomer/jsonResponse/validateUser/userNotCreated.json" file

@userCreated
  Scenario: Validate user created
    When I perform a POST request to the "users/validate" path with body
      | parameterName    | parameterValue |
      | idType           | C              |
      | idDocumentNumber | 201906126      |
      | phone            | 65531649       |
      | country          | CR             |
    Then The response should contain the 200 status code
     And The response should contain the application/json;charset=UTF-8 content type
     And The schema of the response should be equal to the "/unicomer/schemes/validateUser/userCreated.json" file
