@unicomer
@accounts

Feature: Accounts of an user.

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

@createdUser
  Scenario: Login of a user created
    When I log in on Unicomer with the "approvedCredit" user
