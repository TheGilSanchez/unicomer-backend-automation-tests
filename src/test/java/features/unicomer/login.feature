@unicomer
@login_users

Feature: login Unicomer.
  Test all scenarios of logging on Unicomer

  Background: Set the url base
    Given I set the "https://unicomer-dev-cr.yellowpepper.com/unicomer/costa-rica/user-service/" base URL

@userNotCreated
  Scenario: Login of a user not created
     When I generate a unique id
      And I generate a UUI id
      And I perform authentication operation for "users/" path with body
       | parameterName | parameterValue |
       | pin           | 12345567       |
    Then The response should contain the 500 status code
     And The response should contain the application/json;charset=UTF-8 content type
     And The schema of the response should be equal to the "/unicomer/schemes/login/userNotCreated.json" file
     And The JSON response should be equal to the "/unicomer/jsonResponse/login/userNoCreated.json" file

@newUser
  Scenario: Login of a new user
    When I create an user with the following data
      | parameterName  | parameterValue |
      | idDocumentType | C              |
      | devicePlatform | ANDROID        |
      | deviceModel    | HUAWEI         |
      | country        | CR             |
      | pin            | 589437         |
      | extUId         | 0              |
     And I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589437         |
    Then The response should contain the 200 status code
     And The response should contain the application/json;charset=UTF-8 content type
     And The schema of the response should be equal to the "/unicomer/schemes/login/userCreated.json" file

@createdUser
  Scenario: Login of a user created
    When I log in on Unicomer with the "approvedCredit" user
    Then The response should contain the 200 status code
     And The response should contain the application/json;charset=UTF-8 content type

  @wrongPinWithUser
  Scenario: Login with wrong Pin of a user created until it consumes the attempts
    When I create an user with the following data
      | parameterName  | parameterValue |
      | idDocumentType | C              |
      | devicePlatform | ANDROID        |
      | deviceModel    | HUAWEI         |
      | country        | CR             |
      | pin            | 589438         |
      | extUId         | 0              |
    And I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The schema of the response should be equal to the "/unicomer/schemes/login/userCreated.json" file
    And The response should be equal to the 4 attempt
    When I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The response should be equal to the 3 attempt
    When I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The response should be equal to the 2 attempt
    When I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The response should be equal to the 1 attempt
    When I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The response should be equal to the 0 attempt
    When I perform authentication operation for "users/" path with body
      | parameterName | parameterValue |
      | pin           | 589432         |
    Then The response should contain the 200 status code
    And The response should contain the application/json;charset=UTF-8 content type
    And The response should be equal to the PIN_BLOCKED attempt