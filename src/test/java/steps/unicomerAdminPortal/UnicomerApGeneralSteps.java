package steps.unicomerAdminPortal;

import qa.yellowpepper.backend.core.helpers.requestHelpers;
import qa.yellowpepper.backend.core.utilities.constants;
import qa.yellowpepper.backend.core.utilities.globalValues;
import qa.yellowpepper.backend.core.utilities.requestResponse;
import qa.yellowpepper.backend.core.utilities.restAssuredExtension;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import io.restassured.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import unicomerAdminPortal.Constants;

public class UnicomerApGeneralSteps {

    private requestResponse response = new requestResponse();

    @When("^I perform Login operation to AP for \"([^\"]*)\" path with body$")
    public void performLoginPostWithBody(String urlPath, DataTable table) throws JSONException {
        JSONObject body = requestHelpers.createBodyRequest(table);
        String finalUrl = urlPath;
        restAssuredExtension.doPostOpsWithBody(finalUrl, body);
        globalValues.tokenGenerated = response.getBodyResponse().jsonPath().getString("accessToken");
    }

    @When("^I get Users operation to AP for \"([^\"]*)\" path with \"([^\"]*)\"$")
    public void performGetUsers(String urlPath, String sizeNumber) throws JSONException {
        String finalUrl = urlPath + Constants.PAGE_SIZE + sizeNumber;
        Header headerToken = new Header(constants.AUTHORIZATION_PARAMETER_NAME, "Bearer " + globalValues.tokenGenerated);
        restAssuredExtension.doGetWithHeaders(finalUrl, headerToken);
    }
}
