package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.json.simple.parser.ParseException;
import qa.yellowpepper.backend.core.utilities.validateTestCases;

import java.io.IOException;

public class hooks  {
    validateTestCases ValidateTestCases = new validateTestCases();
    private static boolean featureFlag = false;

    @After
    public void after(final Scenario scenario) throws IOException, ParseException {
        if (scenario.isFailed()){
            ValidateTestCases.getStepsErrorMessage(scenario);
        }
        if (!ValidateTestCases.getResultJsonFileAsJSONObject()) {
            ValidateTestCases.createResultFileFrom0(scenario);
        } else {
            if (scenario.getName().equals(ValidateTestCases.parseResults.get("test_case_name"))) {
                ValidateTestCases.reWriteResultFile(scenario);
            } else {
                ValidateTestCases.validateResults(scenario);
                ValidateTestCases.deleteResultFile();
                ValidateTestCases.createResultFileFrom0(scenario);
            }
        }
        if(!featureFlag) {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    ValidateTestCases.getResultJsonFileAsJSONObject();
                    ValidateTestCases.validateResults(scenario);
                    ValidateTestCases.deleteResultFile();
                } catch (IOException | ParseException e) {
                    e.printStackTrace();
                }
            }));
            featureFlag = true;
        }
    }
}
