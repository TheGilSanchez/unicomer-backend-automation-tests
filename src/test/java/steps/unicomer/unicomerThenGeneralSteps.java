package steps.unicomer;

import qa.yellowpepper.backend.core.utilities.requestResponse;
import cucumber.api.java.en.Then;
import org.json.JSONObject;

import static org.junit.Assert.assertEquals;

public class unicomerThenGeneralSteps {

    private requestResponse response = new requestResponse();

    @Then("^The response should be equal to the ([^\"]*) attempt")
    public void validateResponseWithAttempts(String valueToCompare) {
        JSONObject jsonResponse = new JSONObject(response.getBodyResponseAsString());
        String attemptsRemaining = jsonResponse.get("attemptsRemaining").toString();
        String status = jsonResponse.get("status").toString();
        int attempt = Integer.parseInt(attemptsRemaining);
        if (attempt == 0 && status.equals("PIN_BLOCKED"))
        {
            assertEquals("The end status is ", status, valueToCompare);
        }else
            assertEquals("Attempts number to this value is", attemptsRemaining, valueToCompare);
    }
}
