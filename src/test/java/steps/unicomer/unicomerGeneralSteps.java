package steps.unicomer;

import qa.yellowpepper.backend.core.helpers.requestHelpers;
import unicomer.constants;
import qa.yellowpepper.backend.core.utilities.loggerSetup;
import qa.yellowpepper.backend.core.utilities.requestResponse;
import qa.yellowpepper.backend.core.utilities.restAssuredExtension;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.apache.log4j.Level;
import org.json.JSONException;
import org.json.JSONObject;
import unicomer.createUser;
import unicomer.userHelper;

import static qa.yellowpepper.backend.core.utilities.globalValues.*;
public class unicomerGeneralSteps {

    private requestResponse response = new requestResponse();
    private loggerSetup Logger = new loggerSetup();
    private createUser createUser = new createUser();
    private userHelper userHelper = new userHelper();

    @When("^I perform authentication operation for \"([^\"]*)\" path with body$")
    public void performAuthenticationPostWithBody(String urlPath, DataTable table) throws JSONException {
        JSONObject body = requestHelpers.createBodyRequest(table);
        body.put(constants.DEVICE_ID_PARAMETER, uuiId);
        String finalUrl = urlPath + uniqueId + constants.LOGIN_PATH;
        restAssuredExtension.doPostOpsWithBody(finalUrl, body);
    }

    @When("^I perform a create user operation using the \"([^\"]*)\" path with body$")
    public void createUserPostWithBody(String urlPath, DataTable table) throws JSONException {
        JSONObject body = requestHelpers.createBodyRequest(table);
        personFakeJSON.put(constants.DEVICE_ID_PARAMETER, uuiId);
        personFakeJSON.put(constants.UNIQUE_ID_PARAMETER, uniqueId);
        JSONObject mergedJSON = new JSONObject(body, JSONObject.getNames(body));
        for (String jsonMerged : JSONObject.getNames(personFakeJSON)) {
            mergedJSON.put(jsonMerged, personFakeJSON.get(jsonMerged));
        }
        Logger.writerLogger(Level.INFO, unicomerGeneralSteps.class.toString(), String.format("Person fake information generated: %s", mergedJSON));
        restAssuredExtension.doPostOpsWithBody(urlPath, mergedJSON);
    }

    @When("^I create an user with the following data$")
    public void createUser(DataTable table) throws JSONException {
        JSONObject body = requestHelpers.createBodyRequest(table);
        createUser.prepareDataToCreateUser();
        JSONObject mergedJSON = new JSONObject(body, JSONObject.getNames(body));
        for (String jsonMerged : JSONObject.getNames(personFakeJSON)) {
            mergedJSON.put(jsonMerged, personFakeJSON.get(jsonMerged));
        }
        restAssuredExtension.doPostOpsWithBody(constants.CREATE_USER_PATH, mergedJSON);
    }

    @When("^I log in on Unicomer with the \"([^\"]*)\" user$")
    public void logInUser(String user) throws JSONException {
        userHelper.loginUser(user);
    }

    @When("^I get the account in the \"([^\"]*)\" path")
    public void getAccounts(String path) throws JSONException {

    }

    @When("^I perform a create user operation using the \"([^\"]*)\" path with body and empty fields$")
    public void createUserPostWithEmptyFields(String urlPath, DataTable table) throws JSONException {
        JSONObject body = requestHelpers.createBodyRequest(table);
        personFakeJSON.put(constants.DEVICE_ID_PARAMETER, "");
        personFakeJSON.put(constants.UNIQUE_ID_PARAMETER, "");
        JSONObject mergedJSON = new JSONObject(body, JSONObject.getNames(body));
        for (String jsonMerged : JSONObject.getNames(personFakeJSON)) {
            mergedJSON.put(jsonMerged, personFakeJSON.get(jsonMerged));
        }
        Logger.writerLogger(Level.INFO, unicomerGeneralSteps.class.toString(), String.format("Person fake information generated: %s", mergedJSON));
        restAssuredExtension.doPostOpsWithBody(urlPath, mergedJSON);
    }

}
