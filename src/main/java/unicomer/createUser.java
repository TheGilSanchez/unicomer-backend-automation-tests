package unicomer;

import qa.yellowpepper.backend.core.helpers.androidHelpers;
import qa.yellowpepper.backend.core.helpers.personFakeHelper;
import qa.yellowpepper.backend.core.utilities.loggerSetup;
import org.apache.log4j.Level;
import org.json.JSONObject;

import static qa.yellowpepper.backend.core.utilities.globalValues.*;

public class createUser {

    private personFakeHelper personFakeHelper = new personFakeHelper();
    private loggerSetup Logger = new loggerSetup();

    public void prepareDataToCreateUser() {
        androidHelpers.generateUniqueId();
        Logger.writerLogger(Level.INFO, createUser.class.toString(), String.format("Unique Id Generated: %s", uniqueId));
        androidHelpers.generateUUICode();
        Logger.writerLogger(Level.INFO, createUser.class.toString(), String.format("UUI Id Generated: %s", uuiId));
        personFakeHelper.generatePersonalFakeInformation();
        personFakeJSON = new JSONObject(personFakeInformation);
        personFakeJSON.put(constants.DEVICE_ID_PARAMETER, uuiId);
        personFakeJSON.put(constants.UNIQUE_ID_PARAMETER, uniqueId);
    }

    public void logInUser() {

    }
}
