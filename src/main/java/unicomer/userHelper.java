package unicomer;

import qa.yellowpepper.backend.core.utilities.generalUtilities;
import qa.yellowpepper.backend.core.utilities.restAssuredExtension;
import org.json.JSONObject;

public class userHelper {

    private generalUtilities generalUtilities = new generalUtilities();

    /**
     * Get an user of a list of users
     * @param userName String
     * @return JSONObject
     */
    public JSONObject getUser(String userName) {
        JSONObject users = generalUtilities.getJsonFileAsJSONObject(constants.UNICOMER_JSON_PATH);
        return (JSONObject) users.get(userName);
    }

    /**
     * Log in of an user
     * @param userName String
     */
    public void loginUser(String userName) {
        JSONObject body = new JSONObject();
        JSONObject user = getUser(userName);
        body.put(constants.DEVICE_ID_PARAMETER, user.get(constants.DEVICE_ID_PARAMETER));
        body.put(constants.PIN_PARAMETER, user.get(constants.PIN_PARAMETER));
        String url = constants.USER_PATH + user.get("uniqueId") + constants.LOGIN_PATH;
        restAssuredExtension.doPostOpsWithBody(url, body);
    }
}
