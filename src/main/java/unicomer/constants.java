package unicomer;

public class constants {

    final public static String UNICOMER_JSON_PATH = "/unicomer/users.json";
    final public static String DEVICE_ID_PARAMETER = "deviceId";
    final public static String PIN_PARAMETER = "pin";
    final public static String USER_PATH = "users/";
    final public static String LOGIN_PATH = "/login";
    final public static String UNIQUE_ID_PARAMETER = "uniqueId";
    final public static String CREATE_USER_PATH = "users";
}
